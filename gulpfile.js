'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();

// Static server
gulp.task('browser-sync', function() {
  browserSync.init({
    server: {
      baseDir: "./"
    },
    notify: false
  });
});

gulp.task('scss', function() {
  return gulp.src('./scss/**/*.scss')
    .pipe(sass({
      includePaths: ['./node_modules'],
      outputStyle: 'compressed'
    }).on('error', sass.logError))
    .pipe(gulp.dest('./assets/global/css/'));
});

gulp.task('scss:watch', ['scss'], function() {
  gulp.watch('./scss/**/*.scss', ['scss']).on('change', browserSync.reload);
  gulp.watch("./*.html").on('change', browserSync.reload);
});

gulp.task('default', ['browser-sync', 'scss:watch']);
